﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataSet dsClientes;
        private DataTable tblClientes;
        private DataRow drClientes;
        public FrmCliente()
        {
            InitializeComponent();
        }

        public DataTable TblClientes { set { tblClientes = value; } }
        public DataSet DsClientes { set { dsClientes = value; } }

        public DataRow DrCliente
             set
            {
                drCliente = value;
        mskCedula.Text = drClientes["Cedula"].ToString();
        txtNombres.Text = drClientes["Nombre"].ToString();
        txtApellidos.Text = drClientes["Apellido"].ToString();

        mskTel.Text = drClientes["Telefono"].ToString();
        txtCorreo.Text = drClientes["Correo"].ToString();
        txtDireccion.Text = drClientes["Direccion"].ToString();
        
      
        


        private void FrmCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
