﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private double subtotal;
        private double iva;
        private double total;
        private string cod_factura;

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }
        public DataSet DsSistema
        { set { dsSistema = value; } }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbemp.DataSource = dsSistema.Tables["Empleado"];
            cmbemp.DisplayMember = "NA";
            cmbemp.ValueMember = "ID";

            cmbprod.DataSource = dsSistema.Tables["Producto"];
            cmbprod.DisplayMember = "SKUN";
            cmbprod.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvfactura.DataSource = bsProductoFactura;

            cod_factura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtcod.Text = cod_factura;
        }

        private void cmbprod_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProducto = ((DataRowView)cmbprod.SelectedItem).Row;
            txtcant.Text = drProducto["Cantidad"].ToString();
            txtprice.Text = drProducto["Precio"].ToString();
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbprod.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["ID"] = drProducto["ID"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
            }
            catch(ConstraintException)
            {
                MessageBox.Show(this, "ERROR,Producto ya agregado, verifique por favor", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgvfactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgrProductoFactura = dgvfactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);
            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) > Int32.Parse(drProducto["Cantidad"].ToString()))
            {
                MessageBox.Show(this, "ERROR, La cantidad de producto a vender no puede ser mayor que la del almacen", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                drProductoFactura["Cantidad"] = Int32.Parse(drProducto["Cantidad"].ToString());
            }
            CalcularTotalFactura();
        }

        private void CalcularTotalFactura()
        {
            subtotal = 0;
            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }
            iva = subtotal * 0.15;
            total = iva + subtotal;

            txtsubtotal.Text = subtotal.ToString();
            txtiva.Text = iva.ToString();
            txttotal.Text = total.ToString();
        }

        private void dgvfactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularTotalFactura();
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvfactura.SelectedRows;
            if(dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "Error, no hay filas para eliminar", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;
            dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            CalcularTotalFactura();
        }
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private void UpdateData()
        {
            if (cmbemp.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombre = dataRow.Field<String>("Nombre"),
                        Apellido = dataRow.Field<String>("Apellido")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombre.Contains(cmbemp.Text)));
            }
            else
            {
                RestartTimer();
            }
        }


        //Actualizar el combo con nuevos datos
        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbemp.Text;

            if (dataSource.Count() > 0)
            {
                cmbemp.DataSource = dataSource;

                var sText = cmbemp.Items[0].ToString();
                cmbemp.SelectionStart = text.Length;
                cmbemp.SelectionLength = sText.Length - text.Length;
                cmbemp.DroppedDown = true;
                return;
            }
            else
            {
                cmbemp.DroppedDown = false;
                cmbemp.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        //Update data when timer stops
        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void cmbemp_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbemp_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }

        }

        private void cmbemp_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();

        }

        private void btnfacturar_Click(object sender, EventArgs e)
        {
            if (dsSistema.Tables["ProductoFactura"].Rows.Count == 0)
            {
                MessageBox.Show(this, "Error, no se puede generar la factura, revise que hayan productos", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["Codigo_Factura"] = cod_factura;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtob.Text;
            drFactura["Empleado"] = cmbemp.SelectedValue;
            drFactura["SubTotal"] = subtotal;
            drFactura["Iva"] = iva;
            drFactura["Total"] = total;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);

            foreach(DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["ID"];
                drDetalleFactura["Producto"] = dr["ID"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);

                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) - Double.Parse(dr["Cantidad"].ToString());
            }
            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.DsSistema = dsSistema;
            frf.Show();

            Dispose();
        }
    }
}
