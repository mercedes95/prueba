﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataSet DsEmpleados
        {
            set
            {
                dsEmpleados = value;
            }
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgbempleado.DataSource = bsEmpleados;
            dgbempleado.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmEmpleado femp = new FrmEmpleado();
            femp.TblEmpleados = dsEmpleados.Tables["Empleado"];
            femp.DsEmpleados = dsEmpleados;
            femp.ShowDialog();
        }

        private void txtfinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEmpleados.Filter = string.Format("Sku like '*{0}*' or Nombre like '*{0}*' or Descripcion like '*{0}*' ", txtfinder.Text);
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection row = dgbempleado.SelectedRows;
            if(row.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para editar", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = row[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmEmpleado fe = new FrmEmpleado();
            fe.TblEmpleados = dsEmpleados.Tables["Empleado"];
            fe.DsEmpleados = dsEmpleados;
            fe.DrEmpleado = drow;
            fe.ShowDialog();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection row = dgbempleado.SelectedRows;
            if (row.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para eliminar", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = row[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsEmpleados.Tables["Empleado"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
