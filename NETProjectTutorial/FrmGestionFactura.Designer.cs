﻿namespace NETProjectTutorial
{
    partial class FrmGestionFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtfind = new System.Windows.Forms.TextBox();
            this.dgvFactura = new System.Windows.Forms.DataGridView();
            this.btnew = new System.Windows.Forms.Button();
            this.btnsee = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactura)).BeginInit();
            this.SuspendLayout();
            // 
            // txtfind
            // 
            this.txtfind.Location = new System.Drawing.Point(13, 13);
            this.txtfind.Multiline = true;
            this.txtfind.Name = "txtfind";
            this.txtfind.Size = new System.Drawing.Size(409, 20);
            this.txtfind.TabIndex = 0;
            // 
            // dgvFactura
            // 
            this.dgvFactura.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvFactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactura.Location = new System.Drawing.Point(13, 39);
            this.dgvFactura.Name = "dgvFactura";
            this.dgvFactura.Size = new System.Drawing.Size(409, 265);
            this.dgvFactura.TabIndex = 1;
            // 
            // btnew
            // 
            this.btnew.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnew.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnew.Location = new System.Drawing.Point(127, 311);
            this.btnew.Name = "btnew";
            this.btnew.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnew.Size = new System.Drawing.Size(75, 23);
            this.btnew.TabIndex = 2;
            this.btnew.Text = "Nuevo";
            this.btnew.UseVisualStyleBackColor = true;
            this.btnew.Click += new System.EventHandler(this.btnew_Click);
            // 
            // btnsee
            // 
            this.btnsee.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsee.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnsee.Location = new System.Drawing.Point(222, 310);
            this.btnsee.Name = "btnsee";
            this.btnsee.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnsee.Size = new System.Drawing.Size(75, 23);
            this.btnsee.TabIndex = 3;
            this.btnsee.Text = "Ver";
            this.btnsee.UseVisualStyleBackColor = true;
            // 
            // FrmGestionFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 361);
            this.Controls.Add(this.btnsee);
            this.Controls.Add(this.btnew);
            this.Controls.Add(this.dgvFactura);
            this.Controls.Add(this.txtfind);
            this.Name = "FrmGestionFactura";
            this.Text = "FrmGestionFactura";
            this.Load += new System.EventHandler(this.FrmGestionFactura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactura)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtfind;
        private System.Windows.Forms.DataGridView dgvFactura;
        private System.Windows.Forms.Button btnew;
        private System.Windows.Forms.Button btnsee;
    }
}