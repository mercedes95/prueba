﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {

        private int id;
        private string inss;
        private string cedula;
        private string nombre;
        private string apellido;
        private string direccion;
        private string convenconal;
        private string celular;
        private SEXO sexo;
        private double salario;

        public Empleado() { }
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Convenconal
        {
            get
            {
                return convenconal;
            }

            set
            {
                convenconal = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public SEXO Sexo
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        }

        public enum SEXO
        {
            FEMALE, MALE
        }
        public Empleado(int id, string inss, string cedula, string nombre, string apellido, string direccion, string convenconal, string celular, SEXO sexo, double salario)
        {
            this.Id = id;
            this.Inss = inss;
            this.Cedula = cedula;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Direccion = direccion;
            this.Convenconal = convenconal;
            this.Celular = celular;
            this.Sexo = sexo;
            this.Salario = salario;
        }

        public override string ToString()
        {
            return Nombre + "" + Apellido;
        }
    }
}
