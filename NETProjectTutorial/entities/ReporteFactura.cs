﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        //Factura
        private string cod_factura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;

        //Detallefactura
        private int cantidad;
        private double precio;

        //Producto
        private string sku;
        private string nombreprod;

        //Empleado
        private string nombremp;
        private string apellidoemp;

        public ReporteFactura(string cod_factura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nombreprod, string nombremp, string apellidoemp)
        {
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.cantidad = cantidad;
            this.precio = precio;
            this.sku = sku;
            this.nombreprod = nombreprod;
            this.nombremp = nombremp;
            this.apellidoemp = apellidoemp;
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombreprod
        {
            get
            {
                return nombreprod;
            }

            set
            {
                nombreprod = value;
            }
        }

        public string Nombremp
        {
            get
            {
                return nombremp;
            }

            set
            {
                nombremp = value;
            }
        }

        public string Apellidoemp
        {
            get
            {
                return apellidoemp;
            }

            set
            {
                apellidoemp = value;
            }
        }
    }
}
