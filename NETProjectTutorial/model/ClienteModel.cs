﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> Lstclientes = new List<Cliente>();

        public static List<Cliente> GetLstClientes()
        {
            return Lstclientes;
        }

        public static void Populate()
        {
            Cliente[] cliente =
            {
                new Cliente (001, "001-230989-0099G", "Grace", "Woods", "22336677", "gracewoods@outlook.es", "Las Colinas"),
                new Cliente(002, "001-121290-0077U", "Ezra", "Santtini", "22778908","ezrastni@outlook.com", "El Crucero"),
                new Cliente(003, "001-250685-0098K", "Romina", "Hills", "22336754","rominahlls@outlook.es", "Los Robles")
            };
            Lstclientes = cliente.ToList();
        }
    }
}
