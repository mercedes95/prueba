﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lstempleados = new List<Empleado>();

        public static List<Empleado> GetLstEmpleados()
        {
            return Lstempleados;
        }

        public static void Populate()
        {
            //Empleado[] empleados =
            //{
            //    new Empleado(1,"652345-7", "001-101088-1040X", "Lucas", "Pelucas", "La Calzada", "2245-4585", "8896-6958", Empleado.SEXO.MASCULINO, 45500.50),
            //    new Empleado(2,"741236-9", "001-151299-0045M", "Carl", "Hamburg", "Miami", "2369-1125", "8496-6369", Empleado.SEXO.MASCULINO, 57800.50),
            //    new Empleado(3,"123658-2", "001-020469-0040S", "Sam", "Sandwich", "Dubai", "2240-5514", "8461-6973", Empleado.SEXO.FEMENINO, 50000.50)
            //};
            //Lstempleados = empleados.ToList();

            Lstempleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.EmpleadoData));
        }
    }
}
